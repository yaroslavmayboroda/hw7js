'use script'

function filterBy(array, dataType) {
    var filteredArray = [];

    for (var i = 0; i < array.length; i++) {
        if (typeof array[i] !== dataType) {
            filteredArray.push(array[i]);
        }
    }
    return filteredArray;
}

var array = ['hello', 'world', 23, '23', null];
var filteredArray = filterBy(array, 'string');

console.log(filteredArray);